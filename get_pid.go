package main

import "syscall"

func main() {
    pid, _, _ := syscall.Syscall(39, 0, 0, 0)
    println(pid)
}
